#include <iostream>

using namespace std;

struct node {
  int head;
  node *tail;
};

void printAllElements(node *n) {
  if (n != NULL) {
    cout << n->head << endl;
    printAllElements(n->tail);
  }
}

int sumAllElements(node *n) {
  if (n == NULL) return 0;
  return n->head + sumAllElements(n->tail);
}

struct myStruct {
  int firstValue;
  float secondValue;
};

int add(int a, int b) {
  cout << "adding" << endl;
  int c = a + 1;
  int d = c + b - 1;
  d++;
  return a + b;
}

void loopOver(int current, int end) {
  if (current < end) {
    cout << current << endl;
    loopOver(current+1, end);
  }
}

struct IntOrFloatUser {
  virtual double useInt(int x) = 0;
  virtual double useFloat(float x) = 0;
};

enum IntOrFloatTag { isInt, isFloat };

union EitherIntOrFloat {
  int intOption;
  float floatOption;
};

struct IntOrFloatSum {
  IntOrFloatSum(int x) {
    tag = IntOrFloatTag::isInt;
    value.intOption = x;
  }

  IntOrFloatSum(float x) {
    tag = IntOrFloatTag::isFloat;
    value.floatOption = x;
  }

  double match(IntOrFloatUser *f) {
    if (tag == IntOrFloatTag::isInt)
      return f->useInt(value.intOption);
    else
      return f->useFloat(value.floatOption);
  }

private:
  IntOrFloatTag tag;
  EitherIntOrFloat value;
};

struct TestUser1 : IntOrFloatUser {
  private: double storedValue;

public:
  TestUser1(double z) {
    storedValue = z;
  }

  double useInt(int x) override {
    return x + storedValue;
  }

  double useFloat(float x) override {
    return x + 0.2;
  }
};

// IntOrFloatSum addIntsOrFloats(
//   IntOrFloatSum x,
//   IntOrFloatSum y
// ) {
//   if (x.tag == IntOrFloatTag::isInt) {
//     if (y.tag == IntOrFloatTag::isInt) {
//       IntOrFloatSum ret;
//       ret.tag = IntOrFloatTag::isInt;
//       ret.value.intOption = x.value.intOption + y.value.intOption;
//     }
//   }
// }

int main() {
  IntOrFloatSum x(1);
  cout << x.match(new TestUser1(3.5)) << endl;

  node n1;
  n1.head = 1;
  n1.tail = new node;
  n1.tail->head = 2;
  n1.tail->tail = new node;
  n1.tail->tail->head = 3;
  n1.tail->tail->tail = NULL;
  printAllElements(&n1);
}