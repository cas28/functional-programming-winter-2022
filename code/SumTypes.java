// This is a Java version of the C++ code that was presented in our Thursday of
// week 1 lecture. Some minor details of the code are different to account for
// the language difference, but the end result is still that we have a mostly
// "safe" form of pattern matching over an "either int or float" data type.

// Like in the original C++ code, note that we've arbitrarily chosen the types
// "int", "float", and "double" in these examples, but this pattern works with
// *any* Java types. It's also possible to use Java's generic types to
// implement a generalized version of this pattern.

// All Java files must have an outermost class; this is just an organizational
// thing, we're not going to construct any objects of this SumTypes class.
// (We're using it like a namespace in C++.)
class SumTypes {

  // We could actually have more than one tag per type, if we wanted to: for
  // example, we could have an "either Fahrenheit or Celsius" type where each
  // case contains a double value but the two cases are treated differently in
  // computations. For this demo, we have one case that contains an int and one
  // that contains a float.
  enum IntOrFloatTag {
    IntTag,
    FloatTag
  }

  // An IntOrFloatUser value is an object that knows how to use an int to
  // produce a double and knows how to use a float to produce a double.
  // In other words, an IntOrFloatUser object *contains two functions*: one
  // that takes in an int and returns a double, and one that takes in a float
  // and returns a double. Remember, functions are data!
  interface IntOrFloatUser {
    double useInt(int x);
    double useFloat(float x);
  }

  // An IntOrFloatSum object contains either an int or a float, and provides a
  // safe interface for "casing over" these two possibilities.
  static class IntOrFloatSum {

    // We keep the fields of this class private in order to make sure any code
    // that uses an IntOrFloatSum object must use it "safely", in the sense
    // that it cannot accidentally use a float as an int or vice versa.

    // The tag indicates whether the value field contains a float or an int.
    private IntOrFloatTag tag;

    // Java doesn't have the feature of "union types" in the same way that C++
    // does, so we use Object as a general "whatever" type: in Java, *every*
    // type is a subtype of Object. (This explanation glosses over some details
    // about "auto-boxing" and "auto-unboxing" in Java, but they're not
    // necessary to think about for this example.)
    private Object value;

    // The "int case constructor".
    public IntOrFloatSum(int x) {
      this.tag = IntOrFloatTag.IntTag;
      this.value = x;
    }

    // The "float case constructor".
    public IntOrFloatSum(float x) {
      this.tag = IntOrFloatTag.FloatTag;
      this.value = x;
    }

    // This is the only interface we provide for using an IntOrFloatSum value:
    // if we can produce a double using an int, and we can produce a double
    // using a float, then we can produce a double using an IntOrFloatSum.
    // This obviously doesn't cover every possible use case of an "either int
    // or float" value, but it *does* cover every possible way to use an
    // "either int or float" value to produce a double value.
    public double use(IntOrFloatUser f) {

      // In both cases, we use Java's unsafe typecast feature to recover the
      // int or float that was originally stored in the value field. If we get
      // this wrong and try to cast the value to an int when it was originally
      // stored as a float, we get a *runtime error*. The primary goal of this
      // "sum type" pattern is to encapsulate the possibility of this kind of
      // error: *if* this "use" function is defined correctly, *then* all other
      // code that uses IntOrFloatSum values is guaranteed to never throw this
      // kind of runtime error.
      if (this.tag == IntOrFloatTag.IntTag) {
        return f.useInt((int) this.value);
      } else {
        // Try changing this cast to (int) to see the runtime error.
        return f.useFloat((float) this.value);
      }
    }
  }

  // Here's one arbitrary little example of a class that can use an
  // IntOrFloatSum to produce a double. A TestUser1 object *contains* these two
  // function definitions, and at *runtime* the "use" function finds the useInt
  // or useFloat function definition to call by looking inside the TestUser1
  // object's data.
  static class TestUser1 implements IntOrFloatUser {
    public double useInt(int x) {
      return x + 1;
    }

    public double useFloat(float x) {
      return x + 2.3;
    }
  }

  // Here's a slightly more sophisticated IntOrFloatUser implementation, which
  // carries around its own private double in addition to the two function
  // definitions. This allows the code that constructs a TestUser2 object to
  // *customize* the behavior of function definitions inside the TestUser2
  // object, by storing some additional data that those function definitions
  // then refer to.
  static class TestUser2 implements IntOrFloatUser {
    private double offset;

    public TestUser2(double offset) {
      this.offset = offset;
    }

    public double useInt(int x) {
      return x + offset;
    }

    public double useFloat(float x) {
      return x - offset;
    }
  }

  // Here's a little demo of how to construct and use IntOrFloatSum objects.
  public static void main(String[] args) {
    IntOrFloatSum a = new IntOrFloatSum(10);
    IntOrFloatSum b = new IntOrFloatSum(3.14f); // Java floats end in 'f'
    System.out.println(a.use(new TestUser1()));
    System.out.println(b.use(new TestUser1()));
    System.out.println(a.use(new TestUser2(1.23))); // Java doubles don't end in 'f'
    System.out.println(b.use(new TestUser2(4.56)));

    // Java also supports "anonymous classes", which allow us to define a
    // nameless class *within an expression*. This works very nicely with the
    // sum type pattern: the function call below is not actually calling a
    // constructor of the IntOrFloatUser interface (since Java interfaces can't
    // have constructors), it's creating an entirely *new* class that
    // implements IntOrFloatUser and then calling the constructor of *that*
    // class. The definition of this nameless class is created at compile time,
    // and the constructor is called at runtime.
    System.out.println(a.use(new IntOrFloatUser() {
      public double useInt(int x) { return x * 2; }
      public double useFloat(float x) { return x + 10; }
    }));

    // If we want to pass extra data into an object of an anonymous class, we
    // can, as long as we only reference *immutable* variables within the
    // anonymous class definition. Internally, Java compiles this to something
    // very similar to our TestUser2 class definition above, copying the
    // variable's value into the data of the new object. When this happens, we
    // sometimes say the variable is "captured" by the function definition that
    // refers to it.

    // First we'll read a double from console input: try removing "final" from
    // this declaration and modifying the scale variable after its definition
    // to see the compile-time error for capturing mutable variables in an
    // anonymous class definition.
    final double scale = Double.parseDouble(System.console().readLine());

    // Both of these function definitions capture the scale variable.
    System.out.println(b.use(new IntOrFloatUser() {
      public double useInt(int x) { return x * scale; }
      public double useFloat(float x) { return x / scale; }
    }));
  }
}
