module Main where

import Graphs
import System.Environment
import System.FilePath

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filePath] -> do
      input <- readFile filePath
      let graph = adjacencyWordGraph input
      let dotFilePath = dropExtension filePath <.> "dot"
      writeDotFile dotFilePath graph
      output <- predictiveNonsense 100 graph
      putStrLn output
    _ -> putStrLn "usage: cabal exec hw3 -- someTextFile.txt"
