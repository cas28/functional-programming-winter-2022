-- This module explores an implementation of a graph data type, along with a
-- mildly entertaining little application: a low-quality predictive text engine
-- based on randomly exploring a graph of words.

-- If you're not very familiar with graph theory, don't worry! You won't be
-- implementing any search algorithms or anything; your work in this file is
-- mostly going to be to rewrite some of the provided function definitions
-- using higher-order functions.

-- The overall goal of this assignment is to give you exercise in recognizing
-- code patterns that can be written with standard higher-order functions like
-- map and filter. These are the basis for productive functional programming
-- with data structures that act as "collection" or "container" values, like
-- lists, sets, trees, etc.

-- Make sure to review the lecture recordings and notes! This assignment
-- involves several new Haskell concepts since the last assignment, all of
-- which I've demonstrated in lecture.

module Graphs where

-- The fold function is very general, and we'll eventually cover it in its full
-- generality. For now, we have one very specific use for it in this file,
-- which will be explained when it comes up.
import Data.Foldable (fold)

-- We use two fundamental data structures to implement our Graph type: MultiSet
-- and Map. (Remember that "Map" and "map" are generally unrelated - Haskell is
-- annoying about things like this.) The Set type will also be relevant to us
-- as the collection of keys in a Map.

-- A "Map k v" value is a collection that has one slot for each value of type
-- "k", and each slot can be either empty or filled with a value of type "v".
-- This data structure is also known as a dictionary or associative array
-- in other languages, and here it's specifically implemented as a tree map.

-- A "Set a" value is a collection of *unique* values of type "a", meaning a
-- set *cannot* have duplicate values. Data types for sets are often unordered,
-- but "Set" is actually an *ordered* set: internally, the elements are kept
-- sorted, and it's implemented as a tree set.

-- A "MultiSet a" value is a collection of *possibly non-unique* values of type
-- "a", meaning a multiset *can* have duplicate values; in other words, a
-- multiset is a set without the uniqueness requirement. Similar to the Set
-- type, our MultiSet type will be *ordered*. An ordered multiset is similar to
-- a list, but it's optimized to be able to very quickly retrieve the
-- *occurrence count* of each element (the number of duplicates of the element
-- that are in the multiset), and to be fast to combine multiple multisets.

-- (Using an optimized data type instead of a list most likely won't have any
-- noticeable effect on our program's runtime for reasonably-sized inputs,
-- since GHC is very good at automatically optimizing uses of the list type
-- anyway. Still, it's good practice for the sake of getting experience working
-- with collection types in Haskell.)

-- This pair of imports gives us the Map type without a prefix, and all of the
-- other functions from Data.Map.Strict with a "Map." prefix, so that we write
-- (for example) "Map.singleton" instead of just "singleton". This is to avoid
-- name clashes, since the Map, Set, and MultiSet modules share many of the
-- same function names. Ignore the "Strict" part of the module name for now -
-- we'll come back to what that means later in this course.
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

-- Here we do the same for the Set and MultiSet types.
import Data.MultiSet (MultiSet)
import Data.MultiSet qualified as MultiSet
import Data.Set (Set)
import Data.Set qualified as Set

-- We'll be using the GraphViz Dot file format to generate pretty pictures of
-- our graphs. The Dot format is just a simple notation for representing graphs
-- to be displayed as images. See the README for how to generate images from
-- Dot files.

-- The Language.Dot.Syntax module provides the types we'll need to
-- create these files, and the renderDot function will give us the actual
-- string that we write to the file.
import Language.Dot.Pretty (renderDot)
import Language.Dot.Syntax qualified as Dot

-- Finally, to do our random walk through a graph, we need a way to generate
-- random numbers. The System.Random module actually provides quite a lot of
-- functionality, but we're only going to use the randomRIO function, which
-- is an IO action to generate an integer within a given range.
import System.Random (randomRIO)


-- ================
-- EXERCISE 1 START
-- ================

-- Like last time, we're going to warm up with some exercises to make sure you
-- understand the algebra of function types. Here are some little made-up data
-- types for the sake of exercise.

data AB where
  A :: (AB -> AB) -> AB
  B :: AB -> AB

data CD p where
  C :: forall p. (p -> p) -> CD p
  D :: forall p. p -> CD p

data E p where
  E :: forall p. (p -> (p, p)) -> E p

-- Replace each use of "undefined" with an expression of the correct type in
-- the definitions below, so that each definition passes typechecking.
-- DO NOT MODIFY ANY OTHER PART OF THE DEFINITIONS.

-- To be clear, these definitions don't *do* anything in particular: they're
-- just for practice working with the typechecker. There's no real "runnable"
-- code in this exercise.

-- You may ONLY use lambda expressions, tuple constructors (commas within
-- parentheses), and the A, B, C, D, and E constructors in your code for this
-- exercise: no other functions or values.

practice1 :: AB
practice1 = A undefined

practice2 :: AB
practice2 = B undefined

practice3 :: CD AB
practice3 = C undefined

practice4 :: CD ((Bool -> Bool) -> CD Bool)
practice4 = D undefined

practice5 :: forall a. E a
practice5 = E undefined

-- ==============
-- EXERCISE 1 END
-- ==============


-- Now, on to our graph code. In general, a "graph" data type contains a
-- collection of "nodes" and "edges" between those nodes, where each edge has a
-- "source" node and a "target" node.

-- Our Graph type specifically has these properties:
--   - It is *directed*, which means edges are not reversible: an edge from 'a'
--     to 'b' cannot be used as an edge from 'b' to 'a'.
--   - It has *no edge labels*, which means the edges do not carry additional
--     data apart from their source and target node.
--   - It is a *multigraph*, which means there can be more than one edge
--     between the same source and target.
--   - There can be nodes in the graph that are not the source or target of any
--     edges.

-- We define our type of graphs to be polymorphic over the type of nodes in the
-- graph. The keys of the Map include each node that is the *source* of an edge
-- in the graph; the MultiSet of values associated with a key is the multiset
-- of target nodes that have an edge from that key node.
type Graph node = Map node (MultiSet node)

-- For example, here's a little graph constructed with the Map.fromList and
-- MultiSet.toList functions. The first element of each pair is a source node,
-- and the second element of each pair is the multiset of target nodes that
-- have edges coming from that source node. The node labeled 100 is in the
-- graph but is not the source or target of any edges, which is allowed by this
-- Graph type.
exampleGraph :: Graph Int
exampleGraph =
  Map.fromList
    [ (1, MultiSet.fromList [1, 2, 1])
    , (2, MultiSet.fromList [2, 3])
    , (3, MultiSet.fromList [1, 4])
    , (100, MultiSet.fromList [])
    ]

-- To create a Dot file named "test.dot" from this graph, you can run this
-- command in the REPL:
--   writeDotFile "test.dot" exampleGraph

-- Try modifying the exampleGraph definition a few times to change the nodes
-- and edges, and use the Dot file output to make sure you understand how this
-- Graph data type represents a graph value. Throughout this assignment, you
-- may find it helpful to check your understanding visually this way.

-- A Graph may have a node that is the *target* of one or more edges but not
-- the *source* of any edges, in which case it will only appear in one of the
-- *values* of the Map, not as one of the *keys* of the Map. Our graph may have
-- duplicate *edges*, but not duplicate *nodes*, so we return a Set instead of
-- a MultiSet. We use the fold function here to compute the union of all of the
-- sets of target nodes; its type in this use is:
--   fold :: Map node (Set node) -> Set node
nodeSet ::
  forall node. Ord node =>
  Graph node -> Set node
nodeSet graph =
  Set.union
    (Map.keysSet graph)
    (MultiSet.toSet (fold graph))

-- To determine whether a node exists in a graph, we can simply check whether
-- it's a member of the set of all nodes in the graph.
nodeExists ::
  forall node. Ord node =>
  node -> Graph node -> Bool
nodeExists node graph =
  Set.member node (nodeSet graph)

-- One fundamental feature we need when working with graphs is to retrieve the
-- multiset of edges coming out of a given node. This function returns Nothing
-- for an input node that is not in the graph, and otherwise returns Just the
-- MultiSet of target nodes that have edges coming from the input node. Note
-- that we have to consider nodes like 4 in exampleGraph, which *is* in the
-- graph but has zero edges coming out of it; Map.lookup will return Nothing in
-- this case, but edgesFrom returns Just the empty multiset to indicate that
-- the node *is* in the graph even though it is not the *source* of any edges.
edgesFrom ::
  forall node. Ord node =>
  node -> Graph node -> Maybe (MultiSet node)
edgesFrom source graph =
  if Set.member source (nodeSet graph) then
    case Map.lookup source graph of
      Nothing -> Just MultiSet.empty
      Just targets -> Just targets
  else
    Nothing


-- ================
-- EXERCISE 2 START
-- ================

-- To find the number of edges between two nodes in a graph, we check the
-- occurrence count of the target node in the set of edges out of the source
-- node.
edgeCount ::
  forall node. Ord node =>
  Graph node -> node -> node -> Maybe Int
edgeCount graph source target =
  case edgesFrom source graph of
    Nothing -> Nothing
    Just nodes -> Just (MultiSet.occur target nodes)

-- Remember the mapMaybe function from lecture? Here it is again; note how its
-- structure is similar to edgeCount.
mapMaybe :: forall a b. (a -> b) -> Maybe a -> Maybe b
mapMaybe f Nothing = Nothing
mapMaybe f (Just x) = Just (f x)

-- Replace the uses of "undefined" in the edgeCountMapped function definition
-- so that it gives the same output as edgeCount for all possible inputs.

-- DO NOT MODIFY ANY OTHER PART OF THE FUNCTION DEFINITION. Your solution must
-- *not* explicitly use the Just or Nothing constructors or any functions from
-- the Data.Maybe module. Your solution must not call any additional top-level
-- functions that you've defined.

edgeCountMapped ::
  forall node. Ord node =>
  Graph node -> node -> node -> Maybe Int
edgeCountMapped graph source target =
  mapMaybe undefined undefined

-- ==============
-- EXERCISE 2 END
-- ==============


-- Now, let's implement the code that we're using to visualize these Graph
-- values as Dot files. This function calls toDotNotation (defined below) to
-- produce a String containing the Dot file contents, and then uses writeFile
-- to save a file with the given FilePath. See the README for how to use this
-- function to generate a graph image.
writeDotFile ::
  forall node. Show node =>
  FilePath -> Graph node -> IO ()
writeDotFile filePath graph =
  writeFile filePath (toDotNotation graph ++ "\n")

-- The printDotNotation function is an alternative for testing: instead of
-- saving the output to a file, it writes the file contents to the terminal
-- output. This can be convenient for copying and pasting Dot file contents
-- into other applications.
printDotNotation ::
  forall node. Show node =>
  Graph node -> IO ()
printDotNotation graph = putStrLn (toDotNotation graph)

-- The toDotNotation function produces the String contents of a Dot file for a
-- given graph. The toDotGraph function converts our Graph type into a
-- Dot.Graph value describing the structure of a Dot file, and then the
-- renderDot function converts that Dot.Graph value to a String.
toDotNotation ::
  forall node. Show node =>
  Graph node -> String
toDotNotation graph = renderDot (toDotGraph graph)

-- A Dot.Graph value is a structured representation of a Dot file. The
-- Dot.Graph type supports many configuration options for customizing the
-- visual display of our graph, but we're going to keep it simple here: for our
-- purposes, we can think of a Dot.Graph value as a list of "node statements"
-- and "edge statements", which can be constructed with the nodeStatement and
-- edgeStatement functions defined below.

-- The entriesToStatements function does the real work in converting
-- a Graph to a Dot.Graph.
toDotGraph ::
  forall node. Show node =>
  Graph node -> Dot.Graph
toDotGraph graph =
  Dot.Graph
    Dot.UnstrictGraph -- our graph can have self-loops
    Dot.DirectedGraph -- our edges are not always reversible
    Nothing           -- our graph itself has no name
    (entriesToStatements (Map.toList graph))

-- For each node in our graph, this function constructs a "node statement" for
-- the node itself and an "edge statement" for each edge coming out of the
-- node. The edgesToEdgeStatements function (defined below) creates the list of
-- "edge statements" for the current node.
entriesToStatements ::
  forall node. Show node =>
  [(node, MultiSet node)] -> [Dot.Statement]
entriesToStatements [] = []
entriesToStatements ((node, edges) : entries) =
  (nodeStatement node : edgesToEdgeStatements node (MultiSet.toList edges)) ++
    entriesToStatements entries

-- The next couple functions are not important to understand for this
-- assignment, although as usual you're welcome to ask about them if you're
-- curious. These are just configuring some of the layout settings for how our
-- graph will appear as an image.

nodeId ::
  forall node. Show node =>
  node -> Dot.NodeId
nodeId node = Dot.NodeId (Dot.NameId (show node)) Nothing

edgeSourceEntity ::
  forall node. Show node =>
  node -> Dot.Entity
edgeSourceEntity node =
  Dot.ENodeId Dot.NoEdge (nodeId node)

edgeDestinationEntity ::
  forall node. Show node =>
  node -> Dot.Entity
edgeDestinationEntity node =
  Dot.ENodeId Dot.DirectedEdge (nodeId node)

-- The nodeStatement and edgeStatement functions are how we construct the
-- Dot.Statement values that describe each node and edge in our output Dot
-- file. It's not important to understand the implementations of these
-- functions, but make sure you understand their types and how to call them.

nodeStatement ::
  forall node. Show node =>
  node -> Dot.Statement
nodeStatement node =
  Dot.NodeStatement (nodeId node) []

edgeStatement ::
  forall node. Show node =>
  node -> node -> Dot.Statement
edgeStatement source target =
  Dot.EdgeStatement
    [ edgeSourceEntity source
    , edgeDestinationEntity target
    ]
    []


-- ================
-- EXERCISE 3 START
-- ================

-- The edgesToEdgeStatements function takes in a source node and a list of
-- target nodes, which together represent a list of edges from the source node
-- to each target node. The output is a list of Dot.Statement values
-- constructed with edgeStatement, containing one element for each input edge.
edgesToEdgeStatements ::
  forall node. Show node =>
  node -> [node] -> [Dot.Statement]
edgesToEdgeStatements source [] = []
edgesToEdgeStatements source (target : targets) =
  edgeStatement source target :
    edgesToEdgeStatements source targets

-- Remember the mapList function from lecture? Here it is again; note how its
-- structure is similar to edgesToEdgeStatements.
mapList :: forall a b. (a -> b) -> [a] -> [b]
mapList f [] = []
mapList f (x : xs) = f x : mapList f xs

-- Replace the uses of "undefined" in the edgesToEdgeStatementsMapped function
-- definition so that it gives the same output as edgesToEdgeStatements for all
-- possible inputs.

-- DO NOT MODIFY ANY OTHER PART OF THE FUNCTION DEFINITION. Your solution must
-- *not* explicitly use the [] or (:) constructors or any functions from the
-- Data.List module (including "head" and "tail"). Your solution must not call
-- any additional top-level functions that you've defined.

edgesToEdgeStatementsMapped ::
  forall node. Show node =>
  node -> [node] -> [Dot.Statement]
edgesToEdgeStatementsMapped source targets =
  mapList undefined undefined

-- ==============
-- EXERCISE 3 END
-- ==============


-- Now that we have a decent way to visualize our graphs, let's do something
-- fun with them! We're going to build a tiny predictive text engine that uses
-- a graph to represent its "brain". This is known as a "Markov chain", and
-- while it can be applied in relatively sophisticated ways, we're only going
-- to use the idea in a pretty simple way.

-- Many written languages, including English, have grammar rules that mostly
-- depend on the *ordering* of words in a sentence. For languages with this
-- property, it can be interesting to build a graph of *adjacent* words from
-- some text, which captures a small but meaningful part of the grammatical
-- structure of the text.

-- For example, here's an "adjacency word graph" constructed from a piece of
-- text earlier in this document. Try converting it to Dot format and looking
-- at it to get a better idea of what I'm talking about.

exampleWordGraph :: Graph String
exampleWordGraph = adjacencyWordGraph "The nodeStatement and edgeStatement functions are how we construct the Dot.Statement values that describe each node and edge in our output Dot file. It's not important to understand the implementations of these functions, but make sure you understand their types and how to call them."

-- If you look for the starting node ("The"), you can see that there's a path
-- through the graph that produces the original sentence, but there are also
-- alternate paths that produce other outputs; for example, starting on the
-- same node, there's a path that produces the string "The nodeStatement and
-- edgeStatement functions are how to call them."

-- The interesting thing about these graphs is that *sometimes* - not really
-- very often, but more often than choosing words by random chance - any
-- arbitrary path we choose in this graph will produce a grammatically-valid
-- sentence.

-- More generally, if we have a "current word" in some sentence, we can predict
-- the next word by choosing a random edge out of that word's node in the
-- graph. This is one way to implement the kind of predictive text feature you
-- see on cell phones.

-- In an AI or natural language processing class you can study some more
-- serious applications and extensions of this concept, but here we will use
-- this idea in its simplest form to make our computers say silly things to us.
-- Our educational goal is to get practice with higher-order functions and
-- collection types.

-- Our adjacencyWordGraph function splits a string up into a list of words,
-- then converts it to a list of pairs of adjacent words, and finally builds a
-- graph out of it. The fromEdgeList and consecutiveWordPairs functions are
-- defined below, but try playing around with this in a Dot file visualizer to
-- get a feel for how it works.
adjacencyWordGraph :: String -> Graph String
adjacencyWordGraph str =
  fromEdgeList (consecutiveWordPairs str)

-- First, we'll need a way to split up a sentence into a list of adjacent pairs
-- of words. Haskell already gives us a function called "words" that splits a
-- string into a list of words, so we just need a way to take a list of things
-- and turn it into a list of adjacent pairs of things.
consecutiveWordPairs :: String -> [(String, String)]
consecutiveWordPairs str = consecutivePairs (words str)

-- The consecutivePairs function recursively produces a list of adjacent pairs
-- from an input list of any type. You might find it to be a good challenge to
-- try to implement this same behavior with the "foldr" function, which we'll
-- introduce in lecture soon (or already have, depending on when you're reading
-- this).
consecutivePairs :: forall a. [a] -> [(a, a)]
consecutivePairs [] = []
consecutivePairs [x] = []
consecutivePairs (x : y : xs) = (x, y) : consecutivePairs (y : xs)

-- To turn our list of adjacent pairs into a graph, the fromEdgeList function
-- recurses over the list and inserts an edge into the graph for each (source,
-- target) pair. The Map.alter function updates the slot in a Map associated
-- with the given key (in this case "source") with a function: the "entry"
-- argument to the lambda is Nothing if the slot is empty, or Just a multiset
-- of targets if the slot is not empty. The lambda returns Just a new multiset
-- to update the value in the slot, or Nothing to leave the slot empty.
fromEdgeList ::
  forall node. Ord node =>
  [(node, node)] -> Graph node
fromEdgeList [] = Map.empty
fromEdgeList ((source, target) : edges) =
  Map.alter
    (\entry ->
      Just (case entry of
        Nothing -> MultiSet.singleton target
        Just targets -> MultiSet.insert target targets))
    source
    (fromEdgeList edges)


-- Now that we have our adjacency word graph, it's time to stumble randomly
-- through it. There is a little wrinkle: randomness is considered a side
-- effect in Haskell, since a function that returns a single random number is
-- not "pure" in the mathematical sense. In pure mathematics, a "random"
-- function is usually one that returns a *random distribution*, which is a
-- *set* with certain properties - that's quite different than returning a
-- single random number!

-- The easiest way to get a single random number in Haskell is to do it with an
-- IO action. Make sure to review the lecture where we covered the IO type! The
-- IO type allows us to execute the side effect of generating a random number
-- without breaking the promise that an Int value is always deterministic: an
-- IO Int is a *different thing* than an Int.

-- The randomIndex function generates a random number between 0 and the given
-- size argument. We'll only ever call it with positive (nonzero) arguments.
randomIndex :: Int -> IO Int
randomIndex size = randomRIO (0, size - 1)


-- ================
-- EXERCISE 4 START
-- ================

-- With randomIndex, we can pick a random element out of a multiset *if* the
-- multiset is nonempty. The MultiSet.null function checks for the empty case,
-- and in the nonempty case we execute an IO action: generate a random number
-- within the bounds of the multiset, convert the multiset to a list, and
-- retrieve the element at that index in the list using the (!!) list indexing
-- operator. We generally avoid the (!!) operator when possible, but it's
-- really the only reasonable way to get a random element out of this MultiSet
-- type.
randomElement :: forall a. Ord a => MultiSet a -> IO (Maybe a)
randomElement xs =
  if MultiSet.null xs then
    pure Nothing
  else do
    i <- randomIndex (MultiSet.size xs)
    pure (Just (MultiSet.toList xs !! i))

-- Remember the mapIO function from lecture? Here it is again; note how its
-- structure is similar to the "else" branch in randomElement.
mapIO :: forall a b. (a -> b) -> IO a -> IO b
mapIO f action = do
  x <- action
  pure (f x)

-- Replace the use of "undefined" in the randomElementMapped function
-- definition so that it gives the same output as randomElement for all
-- possible inputs.

-- DO NOT MODIFY ANY OTHER PART OF THE FUNCTION DEFINITION. Your solution must
-- *not* explicitly use the "do" keyword or the "<-" operator. Your solution
-- must not call any additional top-level functions that you've defined.
randomElementMapped :: forall a. Ord a => MultiSet a -> IO (Maybe a)
randomElementMapped xs =
  if MultiSet.null xs then
    pure Nothing
  else
    mapIO undefined undefined

-- ==============
-- EXERCISE 4 END
-- ==============


-- With randomElement, we can choose a random node out of a graph. We will use
-- this to choose our starting node for our computer to start its speech with.
randomNode ::
  forall node. Ord node =>
  Graph node -> IO (Maybe node)
randomNode graph =
  randomElement (MultiSet.fromSet (nodeSet graph))

-- We can also choose a random edge out of a given node, again using
-- randomElement to select a random target from the multiset of target nodes
-- coming out of the source node.
randomEdge ::
  forall node. Ord node =>
  node -> Graph node -> IO (Maybe node)
randomEdge source graph =
  case edgesFrom source graph of
    Nothing -> pure Nothing
    Just nodes -> randomElement nodes

-- To walk through a graph randomly, we should be responsible: we take in an
-- Int indicating the maximum number of steps we will take. If we haven't run
-- out of steps yet, we choose a next step at random from our current starting node,
-- continue walking for the rest of our steps, and then put this starting node
-- at the front of our list of steps. Note the use of mapIO to include the
-- starting node in the output list.
randomWalk ::
  forall node. Ord node =>
  Int -> node -> Graph node -> IO [node]
randomWalk maxSteps start graph =
  if maxSteps <= 0 then
    pure [start]
  else do
    nextStep <- randomEdge start graph
    case nextStep of
      Nothing -> pure [start]
      Just nextNode ->
        mapIO
          (\rest -> start : rest)
          (randomWalk (maxSteps - 1) nextNode graph)

-- Finally, our computer can speak its mind! Pass in a maximum word count and a
-- string that has "words" of any kind, and predictiveNonsense will try to
-- generate some plausible text for you. The "unwords" function joins a list of
-- strings together with spaces in between them, as a pseudo-inverse of the
-- "words" function that we originally used to split up our text. The
-- predictiveNonsense function works better with longer input strings, so check
-- the README to see how to use an entire text file as input!
predictiveNonsenseFromString :: Int -> String -> IO String
predictiveNonsenseFromString maxSteps str =
  predictiveNonsense maxSteps (adjacencyWordGraph str)

predictiveNonsense :: Int -> Graph String -> IO String
predictiveNonsense maxSteps graph = do
  maybeStart <- randomNode graph
  case maybeStart of
    Nothing -> pure ""
    Just start -> mapIO unwords (randomWalk maxSteps start graph)

-- ================
-- EXERCISE 5 START
-- ================

-- Remember that one property of our Graph type is that a node can have a
-- "self-loop": an edge whose source and target are both that same node.

-- This is sometimes a desirable property of a graph, but not always. The
-- English language has some grammatical constructions that use repeated words,
-- like in the sentence
--   "Yesterday I told them I had had lunch already."
-- or
--   "I think that that one is the correct answer".
--
-- Our predictive text generator may get caught up in these self-loops for any
-- amount of repetitions, leading to output like "I think that that that that".
-- To avoid this possibility, we can remove all of the self-loops from our
-- graph. (This may not actually make our average output more grammatically
-- correct overall, but it will make it more interesting.)

-- The Map.mapWithKey function updates each entry in a Map using a function
-- that takes both the key and value of the entry as input. Study its type in
-- the REPL: the first argument is a function that takes in *two* arguments.
-- Remember the definition of the Graph type: the *keys* of our Map are nodes,
-- and the *values* are multisets of nodes. You should be able to use this to
-- figure out what the type variables "k", "a", and "b" each represent in the
-- use of Map.mapWithKey below.

-- Replace the use of "undefined" in the removeAllSelfLoops function definition
-- so that it removes all self-loops in the graph without removing or adding
-- any other edges (or removing any nodes).

-- The MultiSet.filter function will be helpful: it works just like the
-- filterList function that we covered in lecture, but for MultiSet values
-- instead of list values.

-- DO NOT MODIFY ANY OTHER PART OF THE FUNCTION DEFINITION. Your solution must
-- not call any additional top-level functions that you've defined.

-- This file has given you several different ways to come up with your own test
-- cases to try out with this function in the REPL. If you don't know how to
-- construct a valid argument to this function, make sure you've read the rest
-- of this module!

removeAllSelfLoops ::
  forall node. Ord node =>
  Graph node -> Graph node
removeAllSelfLoops =
  Map.mapWithKey undefined

-- ==============
-- EXERCISE 5 END
-- ==============
