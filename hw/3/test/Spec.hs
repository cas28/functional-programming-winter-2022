module Main where

import Control.Applicative
import Data.Foldable
import Data.Function
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map (Map)
import Data.Map qualified as Map
import Data.MultiSet (MultiSet)
import Data.MultiSet qualified as MultiSet
import Data.Maybe
import GHC.Generics
import System.Random
import Test.Hspec.Core.Spec
import Test.Hspec.Core.QuickCheck
import Test.Hspec.Core.Runner
import Test.QuickCheck
import Test.QuickCheck.Monadic

import Graphs

instance (Ord a, Arbitrary a) => Arbitrary (MultiSet a) where
  arbitrary = fmap fold $ listOf $ fmap MultiSet.fromSet arbitrary
  shrink = map MultiSet.fromMap . shrink . MultiSet.toMap

removeAllSelfLoopsSpec :: Ord node => Graph node -> Graph node
removeAllSelfLoopsSpec g =
  let
    nodeLoop [] = []
    nodeLoop ((source, targets) : sources) =
      (source, edgeLoop source targets) : nodeLoop sources

    edgeLoop source [] = []
    edgeLoop source (target : targets) =
      if source == target then
        edgeLoop source targets
      else
        target : edgeLoop source targets
  in
    fmap MultiSet.fromList $ Map.fromList $
      nodeLoop $ Map.toList $ fmap MultiSet.toList g

main :: IO ()
main = hspec $ do

  specify "exercise 1" $ property $
    seq practice1 $
    seq practice2 $
    seq practice3 $
    seq practice4 $
    seq practice5 $
    True

  specify "exercise 2" $ property $
    forAll arbitrary $ \(g :: Graph Int, x, y) ->
      edgeCount g x y === edgeCountMapped g x y

  specify "exercise 3" $ property $
    forAll arbitrary $ \(x :: Int, ys) ->
      edgesToEdgeStatements x ys === edgesToEdgeStatementsMapped x ys

  specify "exercise 3" $ property $
    forAll arbitrary $ \(x :: Int, ys) ->
      edgesToEdgeStatements x ys === edgesToEdgeStatementsMapped x ys

  specify "exercise 4" $ property $ monadicIO $
    forAllM arbitrary $ \(xs :: MultiSet Int) -> do
      stdGen <- run $ fmap mkStdGen $ generate arbitrary
      liftA2 (===)
        (run (setStdGen stdGen *> randomElement xs))
        (run (setStdGen stdGen *> randomElementMapped xs))

  specify "exercise 5" $ property $
    forAll arbitrary $ \(g :: Graph Int) ->
      removeAllSelfLoops g === removeAllSelfLoopsSpec g
