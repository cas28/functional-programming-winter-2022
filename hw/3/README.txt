CS 457/557 FUNCTIONAL PROGRAMMING
ASSIGNMENT 3

Welcome to our third assignment! This is our first assignment that involves "real" functional programming: we're going to be practicing with some uses of "map" and "filter" functions to process collections of data in a functional way.

This assignment requires an installation of GHC and Cabal to complete. Refer to the lecture notes and recordings if you still need to install these tools.

As mentioned in lecture, you may find it helpful to use the Debug.Trace module to aid your debugging process. If you add an import for Debug.Trace, please make sure to remove the import and any trace calls before submitting your code, to avoid confusing the grading scripts.

To start the assignment, open src/Graphs.hs and start reading from the top. Read all the comments carefully and ask if anything is unclear!

The cheat sheet of terminal commands and REPL commands from assignment 2 are still relevant in this assignment, so review the assignment 2 README for instructions on how to build, interact with, and test the code in this project.

In order to visualize our graphs in this assignment, we'll be outputting GraphViz Dot files, which many tools can use to display a visual graph. I recommend https://dreampuf.github.io/GraphvizOnline or any other web app for GraphViz visualization, so that you don't have to install anything.

The writeDotFile command in Graphs.hs takes in a graph and saves a .dot file representing the graph; to visualize a graph, use writeDotFile to save it to a .dot file, open the .dot file in a text editor, and copy its contents into a GraphViz visualization application like the one linked above. We will also run through this process in lecture.

The "main" function in app/Main.hs is designed to take the path to a text file as input and output a .dot file along with some predictive text based on the text file. To run the program, use this command after building:

  cabal exec hw3 -- someTextFile.txt

(Note that there are spaces before *and* after the -- part of the command.)

The included frankenstein.txt file contains the public-domain text of the book Frankenstein by Mary Shelley, which can be used as an input text file. Try it out on other text files too! It will work for any file that has chunks of text separated by spaces, so it's not restricted to just English or even just natural languages.

When you're finished with the assignment, submit ONLY your modified Graph.hs file to the Canvas assignment 3 dropbox. PLEASE DO NOT ZIP OR RENAME YOUR FILE.
