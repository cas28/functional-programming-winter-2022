CS 457/557 FUNCTIONAL PROGRAMMING
ASSIGNMENT 1

Welcome to our first assignment! This one is a bit of warmup: we're going to be doing some *pure* programming, but not yet really diving into *functional* programming, which we'll be introducing in lecture very soon.

This assignment has two versions so that it can be completed in C++ or Java. You are only required to complete one of these two versions in order to get full points for the assignment.

You're welcome to complete both versions of the assignment, but you won't get extra credit for it: if you submit both versions, your grade for the assignment will be determined by whichever submission has the higher grade.

There are some code differences between the two versions of the assignment to account for language differences, but both versions of the code implement the same core functionality and both versions have the same four exercises for you to complete.

Regardless of which version of the assignment you choose, your code *must* compile and run without errors. Code that fails to compile will not be graded.

Each exercise is worth five points. You may get partial credit for a solution that compiles but sometimes gives the wrong answer.

PLEASE DO NOT MODIFY THE MAIN FUNCTION in your assignment submission. It may be helpful to modify the main function while you're *testing* your code, but please remember to undo those modifications before *submitting* your code, in order to avoid confusing the grading scripts.

To start the assignment, open either 1.cpp or 1.java and start reading from the top of the file.

When you're finished with the assignment, submit your modified 1.cpp file or the 1.java file to the Canvas assignment 1 dropbox. PLEASE DO NOT ZIP OR RENAME YOUR FILE.
