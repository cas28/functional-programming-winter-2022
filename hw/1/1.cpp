// Please read this whole file very carefully: it's meant to be read from top
// to bottom. I know it will take some serious time and effort to read this
// much text, but I promise I've done my best to make it worth your effort! Ask
// for help if you get stuck, and make sure you understand the provided
// examples before you spend too much time trying to write your own code.

// We are going to write somewhat irresponsible C++ code in this exercise: we
// will not be deallocating any of the memory that we manually allocate. I
// promise it'll be fine! We'll be using a *tiny* amount of memory for a modern
// computer, and the operating system will clean up after us when the program
// exits anyway. It is possible to write code like this while being responsible
// with memory allocation and deallocation, it just takes more effort which
// would distract from the topics that we're focusing on here.

#include <iostream>
#include <string>
#include <climits>

// This kind of "using" statement is not always great practice in a real-world
// C++ codebase, but again, we're not here to write perfect C++ code.
using namespace std;

// This is our type of nodes in a singly-linked list of integers, which is
// defined below.
struct node;

// We use the name "list" as a shorthand for "node const * const", and we use
// NULL to represent an empty list. "node const * const" means that the pointer
// variable itself is immutable *and* the space in memory that it points to is
// immutable. This means if we have a variable defined as "list l = ...", we
// can't do a modification like "l = NULL" *or* a modification like "*l = 0"
// after the initial variable definition.
typedef node const * const list;

// See the main function at the bottom of this file for some examples of how to
// construct list values by hand.

// This is our "linked list of integers" type. Pay close attention to
// terminology here: we're using different meanings of "head" and "tail" than
// the traditional C++ meanings, as we discussed in lecture.
struct node {

  // The "head" is the first value in the list.
  // "int const" means this is an immutable variable.
  int const head;

  // The "tail" is the rest of the list after the first value.
  // Think of a snake: the whole snake is just a head and a tail.
  list tail;

  // We define a constructor function for the node type, which just sets the
  // head and tail to the constant argument values.
  node(int head, list tail) : head(head), tail(tail) { }
};

// The node constructor function acts as a function to *prepend* a new value to
// the *front* of a list. Since our lists are immutable, we don't actually
// modify the input list, we output a modified *copy* of the input list.
// (Again, ignore the potential memory leaks in this code, we're not here to
// study memory management right now.)
list insertFirst(int newHead, list l) {
  return new node(newHead, l);
}

// We can create a single-element list by creating a list with a NULL tail.
list singleton(int onlyValue) {
  return new node(onlyValue, NULL);
}

// Here's an example sum function over our list type.
int sum(list l) {
  if (l == NULL) { // list is empty: it has no head or tail
    return 0; // sum of an empty list is 0
  } else { // list is nonempty: it has a head and a tail
    // list->head is the first value in the list
    // sum(list->tail) is the sum of the rest of the list after the first value
    return l->head + sum(l->tail);
  }
}

// Here are example functions to print every element in a list, recursively and
// iteratively (with a loop). The iterative one is not "pure" by Haskell's
// extreme standards because it involves a loop variable being modified, but
// the list type itself is still immutable, so the function is mostly "pure" by
// a slightly looser standard.

void recursivePrint(list l) {
  if (l != NULL) {
    cout << l->head << endl;
    recursivePrint(l->tail);
  }
}

void iterativePrint(list l) {
  // Note that we're using "node const *", not "node const * const". This is
  // why this code is not *strictly* pure by Haskell's standards: we *are* able
  // to modify the "current" variable itself, even though we're still *not*
  // able to modify the thing it points to.
  for (node const * current = l; current != NULL; current = current->tail)
    cout << current->head << endl;
}

// Here's some more bad C++ code: while you should just about never use the
// goto statement in modern real-world C++ code, it can be useful for
// illustrating what makes loops and recursion similar. Here's a print function
// over lists that doesn't use recursion *or* loops, just labels and gotos.
// Both recursivePrint and iterativePrint can compile to code that looks like
// gotoPrint, if certain compiler optimizations are enabled.
void gotoPrint(list l) {
  // This is going to be irresponsible by C++'s standards, so it's probably
  // fair to expect it to be impure by Haskell's standards.
  node const * current = l;

  // This line below is a label: we can use the command "goto loop" to *jump*
  // execution directly to this line from anywhere else in this function. It's
  // exactly like how labels and jump instructions work in assembly language.
loop:
  if (current != NULL) {
    cout << current->head << endl;
    current = current->tail;
    goto loop; // jump to the "loop" label
  }
}

// Here's a function to *append* a new value to the *end* of a list.
list insertLast(list l, int newLast) {
  if (l == NULL) { // list is empty: it has no head or tail
    return new node(newLast, NULL); // return a single-element list
  } else { // list is nonempty: it has a head and a tail
    // list->head is the first value in the list
    // insertLast(list->tail, newLast) is the rest of the list after the first
    // value, with newLast appended to the end
    return new node(l->head, insertLast(l->tail, newLast));
  }
}

// In order to test our code, we'll need to be able to test lists for equality.
bool equals(list l1, list l2) {
  if (l1 == NULL && l2 == NULL) // both lists are empty
    return true; // an empty list equals an empty list
  else if (l1 != NULL && l2 != NULL) { // both lists are non-empty
    // Two non-empty lists are equal if their heads and tails are equal.
    return l1->head == l2->head && equals(l1->tail, l2->tail);
  } else // one list is empty and the other is non-empty
    return false; // an empty list can never equal a non-empty list
}

// When retrieving values from a list by index, we always run into a bit of a
// frustrating problem - any reasonable index type will include indices that
// are *out of bounds* of the list, and what value do we return in that case?

// In other words, what type should be the "???" return type here?
//   ??? getValueAtIndex(unsigned int index, list l) { ... }

// We could return an int and pick some special int value to represent an
// "error": for example, we could say that a return value of -1 indicates that
// the index was too large. But what if we have a list where -1 is one of the
// values? Then a return value of -1 is ambiguous: it could mean that the index
// was out of bounds, or it could mean that the index was *in* bounds and the
// value at that index was -1.

// We could use an exception, but exception handling is impure even by loose
// standards: throwing and catching exceptions causes side effects observable
// outside the function that does the throwing or catching.

// This is a traditional use for a sum type, like we've seen in lecture. We
// will have one case with *no data* except the tag, representing an error
// result, and one case containing an int value, representing a successful
// result.

// Foreshadowing Haskell a little, we will use the name MaybeInt to refer to
// this "either int or nothing" datatype, and the names Just and Nothing for
// the tags.

enum MaybeIntTag {
  Just, // success: we have an int
  Nothing // failure: the index was out of bounds
};

// This is basically just an explicit way to represent an int value that might
// be "undefined": sizeof(MaybeIntValue) = max(sizeof(int), 0) = sizeof(int)
union MaybeIntValue {
  int justValue;
  struct { } nothingValue; // an empty struct is 0 bytes large
};

// To expand on what we've seen in lecture, we'll use a template here to define
// a *generic* version of a "user" for our sum type. When we write for example
// MaybeIntUser<double>, that refers to an implicit copy of this type
// definition where T is replaced with double:
//   struct MaybeIntUser<double> {
//     virtual double useNothing() = 0;
//     virtual double useJust(int x) = 0;
//   }
template <typename T>
struct MaybeIntUser {
  virtual T useNothing() = 0;
  virtual T useJust(int x) = 0;
};

// This class captures *all* possible use cases for an immutable "either int or
// nothing" type, by using a template to allow the "use" function to work for
// all possible return types. This pattern will require some kind of strange
// code flow, though.
class MaybeInt {
  MaybeIntTag const tag;
  MaybeIntValue const value;

public:
  // The "Just case constructor": we have an int.
  // { .justValue = x } constructs a MaybeIntValue with a justValue field.
  MaybeInt(int x) : tag(MaybeIntTag::Just), value({ .justValue = x }) { }

  // The "Nothing case constructor": we have no int.
  // { .nothingValue = { } } constructs a MaybeIntValue with a nothingValue field.
  MaybeInt() : tag(MaybeIntTag::Nothing), value({ .nothingValue = { } }) { }

  // We use a template here too: if we have a MaybeIntUser<T>, we have a way to
  // produce a T by using the private data of a MaybeInt. When we call the use
  // method, we can specify what T should be by writing it like
  // "x.use<double>(...)", but the part in angle brackets is not required if
  // the compiler can figure it out from context: we could just write
  // "x.use(...)", leaving out the whole angle bracket part.
  template <typename T>
  T use(MaybeIntUser<T> *f) {
    if (tag == MaybeIntTag::Just)
      return f->useJust(value.justValue);
    else
      return f->useNothing();
  }
};

// For convenience, we'll overload the << operator to support writing MaybeInt
// values to streams like cout. To do this, we'll create a MaybeIntUser<string>
// object: an object that knows how to produce a string given no data, and
// knows how to produce a string given an int.

struct ToStringUser : public MaybeIntUser<string> {
  string useNothing() override { return "Nothing"; }
  string useJust(int x) override { return "Just " + std::to_string(x); }
};

ostream& operator <<(ostream &out, MaybeInt x) {
  // The <string> here is optional because the compiler knows that ToStringUser
  // only inherits from MaybeIntUser<string>.
  return out << x.use<string>(new ToStringUser);
}

// We'll also overload the == operator, since we'll need a way to compare
// MaybeInt equality in order to test our list functions. This will require a
// careful design for our MaybeIntUser subtype.

// This MaybeIntUser returns true if the MaybeInt is a Nothing value, and
// returns false if it's a Just value.
struct NothingEqualsUser : public MaybeIntUser<bool> {
  bool useNothing() override { return true; }
  bool useJust(int x) override { return false; }
};

// This MaybeIntUser returns true if the MaybeInt is a Just value that equals
// the stored int, and returns False if it's a different Just value or a
// Nothing value.
class JustEqualsUser : public MaybeIntUser<bool> {
  int value;

public:
  JustEqualsUser(int value) : value(value) { }
  bool useNothing() override { return false; }
  bool useJust(int x) override { return x == value; }
};

// This MaybeIntUser returns true if the stored MaybeInt is equal to the given
// MaybeInt, and false otherwise.
class EqualsUser : public MaybeIntUser<bool> {
  MaybeInt value;

public:
  EqualsUser(MaybeInt value) : value(value) { }

  bool useNothing() {
    // If the input is Nothing, check if the stored value is also Nothing.
    return value.use(new NothingEqualsUser);
  }

  bool useJust(int x) {
    // If the input is Just with some int value, check if the stored value is
    // also Just with the same int value.
    return value.use(new JustEqualsUser(x));
  }
};

// Finally, we can define the == operator in terms of EqualsUser.
bool operator ==(MaybeInt x, MaybeInt y) {
  // y is the "stored value" and x is the "input" in the EqualsUser definition
  return x.use(new EqualsUser(y));
}

// With MaybeInt we have what we need to define our getValueAtIndex function.
// Remember, the goal of this was to have a *safe* and *unambiguous* way to
// indicate an error if the index is out of bounds: this is what the MaybeInt()
// constructor does for us.
MaybeInt getValueAtIndex(unsigned int index, list l) {
  if (l == NULL) // list is empty: index is out of bounds
    return MaybeInt(); // Nothing case: we have no int
  else if (index == 0) // list is nonempty and index is at the head
    return MaybeInt(l->head); // Just case: we have an int
  else // list is nonempty and index is in the tail
    return getValueAtIndex(index-1, l->tail); // recursive case: we don't know yet
}



// EXERCISE 1.
// Implement the "append" function to append the whole list l2 at the end of
// l1, so that the resulting list has all of the elements of l1 followed by all
// of the elements in l2. Like in the examples above, instead of modifying the
// original lists, you should output a new list with the elements of l1
// appended to l2.

// Your code does not have to be pure, but do not modify any code outside the
// body of the "append" function.

list append(list l1, list l2) {
  // put your code here
}


// EXERCISE 2.
// Implement the "pairwiseAdd" function to return the sum of each parallel pair
// of elements in the lists l1 and l2.

// For example:
// pairwiseAdd(
//   new node(1, new node(2, singleton(3))),
//   new node(90, new node(80, singleton(70))))
// should return
//   new node(91, new node(82, singleton(73)))

// If the lists are not the same length, ignore the extra elements.

// For example:
// pairwiseAdd(
//   new node(1, new node(2, singleton(3))),
//   new node(90, new node(80, new node(70, new node(60, singleton(50))))))
// should also return
//   new node(91, new node(82, singleton(73)))

// Your code does not have to be pure, but do not modify any code outside the
// body of the "pairwiseAdd" function.

list pairwiseAdd(list l1, list l2) {
  // put your code here
}


// EXERCISE 3.
// Implement the "cumulativeSum" function to return a list where the value at
// each index is the sum of every value up to and including that index in the
// original list.

// In general, if you have an input list that looks like
//   new node(x1, new node(x2, new node(x3, ...)))
// the output should look like
//   new node(x1, new node(x1+x2, new node(x1+x2+x3, ...)))

// For example, if the input is
//   new node(1, new node(20, new node(300, new node(4000, singleton(50000)))))
// the output should be
//   new node(1, new node(21, new node(321, new node(4321, singleton(54321)))))

// If the input is an empty list, the output should be an empty list.

// Your code should be *fully* pure: if you modify any variables after creation
// you will not get full points. This means you'll need to use recursion.

// You may create additional helper functions for this problem. Do not
// modify any code in this file except for adding new helper function
// definitions and implementing the cumulativeSum definition.

list cumulativeSum(list l) {
  // put your code here
}


// EXERCISE 4.
// Implement the "findIndexOfValue" function to return the first (leftmost)
// index of the given value in the list.

// For example,
//   findIndexOfValue(11, new node(10, new node(11, new node(12, singleton(11)))))
// should return
//   MaybeInt(1)

// You can assume that the length of the list is less than or equal to MAX_INT.

// Your code should be *fully* pure: if you modify any variables after creation
// you will not get full points. This means you'll need to use recursion.

// You will need to create a subtype of MaybeIntUser<MaybeInt> in order to
// complete this problem. Do not modify any code in this file except for adding
// this new type definition and implementing the findIndexOfValue definition.
// Also, do not use the toString function of the MaybeInt type.

MaybeInt findIndexOfValue(int valueToFind, list l) {
  // put your code here
}



// MAIN FUNCTION AND TEST CODE

// This is a small utility function for the test code below.
void test(string testCase, bool test) {
  if (!test)
    cout << testCase << ": failed" << endl;
}

// Here's some concrete examples of how to construct and use the data types and
// functions in this file. This main function also contains the automated tests
// for your code: if you pass all these tests *and follow all the instructions*,
// then you'll get 100% for the assignment. You are required to implement the
// algorithms as specified so that they work correctly for all possible inputs
// of the correct type, not just the specific test cases below.
int main() {
  // Feel free to add your own code here for your own testing, but please
  // remove it (or comment it out) before you submit your code so that it
  // doesn't interfere with the automated testing script. (You won't lose
  // points if you forget to do this, but you might make the person grading
  // your code grumpy.)

  // This is the list [1,2,3,4].
  list oneToFour = new node(1, new node(2, new node(3, singleton(4))));

  cout << "oneToFour =" << endl;
  recursivePrint(oneToFour);
  cout << "getValueAtIndex(1, oneToFour) = " << getValueAtIndex(1, oneToFour) << endl;
  cout << "getValueAtIndex(5, oneToFour) = " << getValueAtIndex(5, oneToFour) << endl;
  cout << endl;

  // Here's a semi-arbitrary list of values that we'll use for testing. Again,
  // your code must implement the algorithms as specified for all possible
  // values of the correct type, not just for this particular set of values.
  list testInts =
    new node(INT_MIN, new node(INT_MIN+1, new node(INT_MAX, new node(INT_MAX-1,
      new node(-2, new node(-1, new node(0, new node(1, new node(2, new node(3,
        new node(-1, new node(1234567, new node(INT_MAX-1, new node(-1234567,
          NULL))))))))))))));

  // An example of *property testing*, which we'll see much more of in Haskell:
  // For any int value and any list, if we prepend the int to the front of the
  // list and get the value at index 0, we should get that same int value back.
  // We'll test this for a range of integers over a small selection of lists.
  cout << "testing getValueAtIndex" << endl;
  for (node const *current = testInts; current != NULL; current = current->tail) {
    int const i = current->head;
    test("empty list",
      getValueAtIndex(0, insertFirst(i, NULL)) == MaybeInt(i));

    test("singleton list containing 0",
      getValueAtIndex(0, insertFirst(i, singleton(0))) == MaybeInt(i));

    test("singleton list containing " + std::to_string(i),
      getValueAtIndex(0, insertFirst(i, singleton(i))) == MaybeInt(i));

    test("arbitrary five-element list",
      getValueAtIndex(0, insertFirst(i, new node(1, new node(-100, new node(-654321, new node(0, singleton(2))))))) == MaybeInt(i));
  }
  cout << endl;

  // To test problem 1, we'll split testInts into two lists and append those
  // two lists together, and check that the result we get is equal to testInts.
  // We'll try every possible index to split at, including the two edge cases:
  //   prefix = NULL, suffix = testInts
  //   prefix = testInts, suffix = NULL
  // This gives us a total of *15* cases to test for a list of length 14.
  cout << "testing append (problem 1)" << endl;
  node const *prefix = NULL, *suffix = testInts;
  for (int i = 0; i < 15; i++) {
    test("splitting testInts at index " + std::to_string(i),
      equals(append(prefix, suffix), testInts));

    if (suffix) { // false on the last loop iteration
      prefix = insertLast(prefix, suffix->head);
      suffix = suffix->tail;
    }
  }

  // To test problems 2, 3, and 4, we'll just hand-write a few test cases to
  // cover the edge cases and a couple arbitrary cases. Remember, your code
  // must work as specified for *all* possible inputs, not just these!
  //
  cout << "testing pairwiseAdd (problem 2)" << endl;

  test("both lists empty",
    equals(pairwiseAdd(NULL, NULL), NULL));

  test("l1 empty, l2 singleton",
    equals(pairwiseAdd(NULL, singleton(1)), NULL));

  test("l1 singleton, l2 empty",
    equals(pairwiseAdd(singleton(1), NULL), NULL));

  test("l1 three-element, l2 empty",
    equals(pairwiseAdd(new node(-1, new node(0, singleton(1))), NULL), NULL));

  test("l1 empty, l2 three-element",
    equals(pairwiseAdd(NULL, new node(-1, new node(0, singleton(1)))), NULL));

  test("l1 singleton, l2 singleton",
    equals(pairwiseAdd(singleton(-100), singleton(1)), singleton(-99)));

  test("l1 singleton, l2 three-element",
    equals(pairwiseAdd(singleton(-100), new node(-1, new node(0, singleton(1)))), singleton(-101)));

  test("l1 three-element, l2 singleton",
    equals(pairwiseAdd(singleton(-100), new node(-1, new node(0, singleton(1)))), singleton(-101)));

  test("both lists five-element",
    equals(
      pairwiseAdd(
        new node(1, new node(2, new node(3, new node(4, singleton(5))))),
        new node(10, new node(20, new node(30, new node(40, singleton(50)))))
      ),
      new node(11, new node(22, new node(33, new node(44, singleton(55)))))));

  test("l1 five-element, l2 six-element",
    equals(
      pairwiseAdd(
        new node(1, new node(2, new node(3, new node(4, singleton(5))))),
        new node(10, new node(20, new node(30, new node(40, new node(50, singleton(60))))))
      ),
      new node(11, new node(22, new node(33, new node(44, singleton(55)))))));

  test("l1 six-element, l2 five-element",
    equals(
      pairwiseAdd(
        new node(1, new node(2, new node(3, new node(4, new node(5, singleton(6)))))),
        new node(10, new node(20, new node(30, new node(40, singleton(50)))))
      ),
      new node(11, new node(22, new node(33, new node(44, singleton(55)))))));


  cout << "testing cumulativeSum (problem 3)" << endl;

  test("empty list",
    equals(cumulativeSum(NULL), NULL));

  test("negative singleton list",
    equals(cumulativeSum(singleton(-1)), singleton(-1)));

  test("zero singleton list",
    equals(cumulativeSum(singleton(0)), singleton(0)));

  test("positive singleton list",
    equals(cumulativeSum(singleton(1)), singleton(1)));

  test("arbitrary three-element list",
    equals(
      cumulativeSum(new node(1, new node(20, singleton(300)))),
      new node(1, new node(21, singleton(321)))));

  test("arbitrary fourteen-element list",
    equals(
      cumulativeSum(testInts),
      new node(-2147483648, new node(1, new node(-2147483648, new node(-2,
        new node(-4, new node(-5, new node(-5, new node(-4, new node(-2,
          new node(1, new node(0, new node(1234567, new node(-2146249083,
            singleton(2147483646))))))))))))))));


  cout << "testing findIndexOfValue (problem 4)" << endl;

  test("empty list, negative value",
    findIndexOfValue(-1, NULL).use(new NothingEqualsUser));

  test("empty list, zero value",
    findIndexOfValue(0, NULL).use(new NothingEqualsUser));

  test("empty list, positive value",
    findIndexOfValue(1, NULL).use(new NothingEqualsUser));

  test("singleton list, found negative value",
    findIndexOfValue(-1, singleton(-1)).use(new JustEqualsUser(0)));

  test("singleton list, found positive value",
    findIndexOfValue(1, singleton(1)).use(new JustEqualsUser(0)));

  test("singleton list, not found minimum value",
    findIndexOfValue(INT_MIN, singleton(0)).use(new NothingEqualsUser()));

  test("three-element list, found value",
    findIndexOfValue(5, new node(1, new node(5, singleton(10)))).use(new JustEqualsUser(1)));

  test("three-element list, not found value",
    findIndexOfValue(8, new node(1, new node(5, singleton(10)))).use(new NothingEqualsUser()));

  test("fourteen-element list, found value near start",
    findIndexOfValue(INT_MIN+1, testInts).use(new JustEqualsUser(1)));

  test("fourteen-element list, found value near middle",
    findIndexOfValue(0, testInts).use(new JustEqualsUser(6)));

  test("fourteen-element list, found value near end",
    findIndexOfValue(1234567, testInts).use(new JustEqualsUser(11)));

  test("fourteen-element list, found duplicate value",
    findIndexOfValue(INT_MAX-1, testInts).use(new JustEqualsUser(3)));

  test("fourteen-element list, not found value",
    findIndexOfValue(54321, testInts).use(new NothingEqualsUser()));
}
