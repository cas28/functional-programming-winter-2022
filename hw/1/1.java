// Please read this whole file very carefully: it's meant to be read from top
// to bottom. I know it will take some serious time and effort to read this
// much text, but I promise I've done my best to make it worth your effort! Ask
// for help if you get stuck, and make sure you understand the provided
// examples before you spend too much time trying to write your own code.

// This is just an outer class to contain everything: we'll never instantiate
// any objects of this Assignment1 class.
class Assignment1 {
  // This is our *immutable* "linked list of integers" type. Pay close
  // attention to terminology here: we're using different meanings of "head"
  // and "tail" than the traditional C++ meanings, as we discussed in lecture.
  // This class is defined as static to guarantee that it does not depend on
  // any instance variables of an Assignment1 object, which Java does allow in
  // non-static nested classes.
  static class List {
    public final int head;
    public final List tail;

    public List(final int head, final List tail) {
      this.head = head;
      this.tail = tail;
    }
  }

  // We represent an empty List with the null value. See the main function at
  // the bottom of this file for some examples of how to construct list values
  // by hand.

  // Somewhat non-traditionally for Java code, we'll implement most of our
  // functions over the List type as *static* functions in our outermost
  // Assignment1 class, instead of *instance* methods in the List class. This
  // is to place emphasis on the fact that we're not really doing
  // object-oriented programming, at least conceptually. We'll also mark all of
  // our function arguments and most of our local variables as "final" to
  // indicate that they are immutable.

  // The List constructor function acts as a function to *prepend* a new value
  // to the *front* of a list. Since our lists are immutable, we don't actually
  // modify the input list, we output a modified *copy* of the input list.
  // There is no computational reason to use this function instead of the List
  // constructor, it's just for demonstration.
  static List insertFirst(final int newHead, final List l) {
    return new List(newHead, l);
  }

  // We can create a single-element list by creating a list with a null tail.
  static List singleton(final int onlyValue) {
    return new List(onlyValue, null);
  }

  // Here's an example sum function over our list type.
  static int sum(final List l) {
    if (l == null) { // list is empty: it has no head or tail
      return 0; // sum of an empty list is 0
    } else { // list is nonempty: it has a head and a tail
      // list.head is the first value in the list
      // sum(list.tail) is the sum of the rest of the list after the first value
      return l.head + sum(l.tail);
    }
  }

  // Here are example functions to print every element in a list, recursively and
  // iteratively (with a loop). The iterative one is not "pure" by Haskell's
  // extreme standards because it involves a loop variable being modified, but
  // the list type itself is still immutable, so the function is mostly "pure" by
  // a slightly looser standard.

  static void recursivePrint(final List l) {
    if (l != null) {
      System.out.println(l.head);
      recursivePrint(l.tail);
    }
  }

  static void iterativePrint(final List l) {
    // Note that our "current" variable is *not* final. This is why this code
    // is not *strictly* pure by Haskell's standards: we *are* able to modify
    // the "current" variable itself, even though we're still *not* able to
    // modify its fields.
    for (List current = l; current != null; current = current.tail)
      System.out.println(current.head);
  }

  // In C++ there's a third way to write this print function, without loops
  // *or* recursion, by using a goto statement. Java has no goto statement (for
  // good reason), so we can't show that example in this file, but you may find
  // it helpful to look at the goto-based version of this function in the C++
  // version of this assignment code.

  // Here's a function to *append* a new value to the *end* of a list.
  static List insertLast(final List l, final int newLast) {
    if (l == null) { // list is empty: it has no head or tail
      return new List(newLast, null); // return a single-element list
    } else { // list is nonempty: it has a head and a tail
      // list.head is the first value in the list
      // insertLast(list.tail, newLast) is the rest of the list after the first
      // value, with newLast appended to the end
      return new List(l.head, insertLast(l.tail, newLast));
    }
  }

  // In order to test our code, we'll need to be able to test lists for equality.
  static boolean equals(List l1, List l2) {
    if (l1 == null && l2 == null) // both lists are empty
      return true; // an empty list equals an empty list
    else if (l1 != null && l2 != null) { // both lists are non-empty
      // Two non-empty lists are equal if their heads and tails are equal.
      return l1.head == l2.head && equals(l1.tail, l2.tail);
    } else // one list is empty and the other is non-empty
      return false; // an empty list can never equal a non-empty list
  }

  // When retrieving values from a list by index, we always run into a bit of a
  // frustrating problem - any reasonable index type will include indices that
  // are *out of bounds* of the list, and what value do we return in that case?

  // In other words, what type should be the "???" return type here?
  //   static ??? getValueAtIndex(final int index, final List l) { ... }

  // We could return an int and pick some special int value to represent an
  // "error": for example, we could say that a return value of -1 indicates that
  // the index was too large. But what if we have a list where -1 is one of the
  // values? Then a return value of -1 is ambiguous: it could mean that the index
  // was out of bounds, or it could mean that the index was *in* bounds and the
  // value at that index was -1.

  // We could use an exception, but exception handling is impure even by loose
  // standards: throwing and catching exceptions causes side effects observable
  // outside the function that does the throwing or catching.

  // This is a traditional use for a sum type, like we've seen in lecture. We
  // will have one case with *no data* except the tag, representing an error
  // result, and one case containing an int value, representing a successful
  // result.

  // Foreshadowing Haskell a little, we will use the name MaybeInt to refer to
  // this "either int or nothing" datatype, and the names Just and Nothing for
  // the tags.

  enum MaybeIntTag {
    Just, // success: we have an int
    Nothing // failure: the index was out of bounds
  }

  // To expand on what we've seen in lecture, we'll use a *generic type* here
  // to define a generic version of a "user" for our sum type. When we write
  // for example MaybeIntUser<Double>, that refers to an implicit copy of this
  // type definition where T is replaced with Double:
  //   interface MaybeIntUser<Double> {
  //     Double useNothing();
  //     Double useJust(int x);
  //   }
  // When we use primitive types with generic types, we have to use the
  // "capitalized" class version of the type: for example "Double" instead of
  // "double". We can generally ignore the difference if we don't care about
  // optimal performance: Java will automatically convert between the
  // "capitalized" and "lowercase" versions of primitive types for us.
  interface MaybeIntUser<T> {
    T useNothing();
    T useJust(int x);
  };

  // This class captures *all* possible use cases for an immutable "either int or
  // nothing" type, by using a template to allow the "use" function to work for
  // all possible return types. This pattern will require some kind of strange
  // code flow, though.
  static class MaybeInt {
    final MaybeIntTag tag;

    // We use the Integer type as our internal "either int or null" type. This
    // does incur some auto-boxing and auto-unboxing overhead, but again, we're
    // not concerned with optimal performance in this code.
    final Integer value;

    // The "Just case constructor": we have an int.
    public MaybeInt(int x) {
      this.tag = MaybeIntTag.Just;
      this.value = x;
    }

    // The "Nothing case constructor": we have no int.
    public MaybeInt() {
      this.tag = MaybeIntTag.Nothing;
      this.value = null;
    }

    // We use a generic type here too: if we have a MaybeIntUser<T>, we have a
    // way to produce a T by using the private data of a MaybeInt. When we call
    // the use method, we can specify what T should be by writing it like
    // "x.use<Double>(...)", but the part between angle brackets is not
    // required if the compiler can figure it out from context: we could just
    // write "x.use<>(...)", leaving out the part *between* the angle brackets.
    public <T> T use(MaybeIntUser<T> f) {
      if (tag == MaybeIntTag.Just)
        return f.useJust(value);
      else
        return f.useNothing();
    }

    // For convenience, we'll overload the Object.toString function to support
    // automatically converting MaybeInt values to strings. To do this, we'll
    // create a MaybeIntUser<String> object: an object that knows how to
    // produce a String given no data, and knows how to produce a String given
    // an int.

    private static class ToStringUser implements MaybeIntUser<String> {
      public String useNothing() { return "Nothing"; }
      public String useJust(int x) { return "Just " + x; }
    }

    public String toString() {
      return use(new ToStringUser());
    }
  }

  // We'll also implement an equality function for our MaybeInt type, since
  // we'll need a way to compare MaybeInt equality in order to test our list
  // functions. This will require a careful design for our MaybeIntUser
  // subtype.

  // This MaybeIntUser returns true if the MaybeInt is a Nothing value, and
  // returns false if it's a Just value.
  static class NothingEqualsUser implements MaybeIntUser<Boolean> {
    public Boolean useNothing() { return true; }
    public Boolean useJust(int x) { return false; }
  }

  // This MaybeIntUser returns true if the MaybeInt is a Just value that equals
  // the stored int, and returns False if it's a different Just value or a
  // Nothing value.
  static class JustEqualsUser implements MaybeIntUser<Boolean> {
    private final int value;

    public JustEqualsUser(int value) { this.value = value; }
    public Boolean useNothing() { return false; }
    public Boolean useJust(int x) { return x == value; }
  }

  // This MaybeIntUser returns true if the stored MaybeInt is equal to the given
  // MaybeInt, and false otherwise.
  static class EqualsUser implements MaybeIntUser<Boolean> {
    private final MaybeInt value;

    public EqualsUser(MaybeInt value) { this.value = value; }

    public Boolean useNothing() {
      // If the input is Nothing, check if the stored value is also Nothing.
      return value.use(new NothingEqualsUser());
    }

    public Boolean useJust(int x) {
      // If the input is Just with some int value, check if the stored value is
      // also Just with the same int value.
      return value.use(new JustEqualsUser(x));
    }
  }

  // Finally, we can define an "equals" function in terms of EqualsUser.
  static boolean equals(MaybeInt x, MaybeInt y) {
    // y is the "stored value" and x is the "input" in the EqualsUser definition
    return x.use(new EqualsUser(y));
  }

  // With MaybeInt we have what we need to define our getValueAtIndex function.
  // Remember, the goal of this was to have a *safe* and *unambiguous* way to
  // indicate an error if the index is out of bounds: this is what the MaybeInt()
  // constructor does for us.
  static MaybeInt getValueAtIndex(int index, List l) {
    if (l == null) // list is empty: index is out of bounds
      return new MaybeInt(); // Nothing case: we have no int
    else if (index == 0) // list is nonempty and index is at the head
      return new MaybeInt(l.head); // Just case: we have an int
    else // list is nonempty and index is in the tail
      return getValueAtIndex(index-1, l.tail); // recursive case: we don't know yet
  }



  // EXERCISE 1.
  // Implement the "append" function to append the whole list l2 at the end of
  // l1, so that the resulting list has all of the elements of l1 followed by all
  // of the elements in l2. Like in the examples above, instead of modifying the
  // original lists, you should output a new list with the elements of l1
  // appended to l2.

  // Your code does not have to be pure, but do not modify any code outside the
  // body of the "append" function.

  static List append(final List l1, final List l2) {
    return null; // replace this line with your code
  }


  // EXERCISE 2.
  // Implement the "pairwiseAdd" function to return the sum of each parallel pair
  // of elements in the lists l1 and l2.

  // For example:
  // pairwiseAdd(
  //   new List(1, new List(2, singleton(3))),
  //   new List(90, new List(80, singleton(70))))
  // should return
  //   new List(91, new List(82, singleton(73)))

  // If the lists are not the same length, ignore the extra elements.

  // For example:
  // pairwiseAdd(
  //   new List(1, new List(2, singleton(3))),
  //   new List(90, new List(80, new List(70, new List(60, singleton(50))))))
  // should also return
  //   new List(91, new List(82, singleton(73)))

  // Your code does not have to be pure, but do not modify any code outside the
  // body of the "pairwiseAdd" function.

  static List pairwiseAdd(final List l1, final List l2) {
    return null; // replace this line with your code
  }


  // EXERCISE 3.
  // Implement the "cumulativeSum" function to return a list where the value at
  // each index is the sum of every value up to and including that index in the
  // original list.

  // In general, if you have an input list that looks like
  //   new List(x1, new List(x2, new List(x3, ...)))
  // the output should look like
  //   new List(x1, new List(x1+x2, new List(x1+x2+x3, ...)))

  // For example, if the input is
  //   new List(1, new List(20, new List(300, new List(4000, singleton(50000)))))
  // the output should be
  //   new List(1, new List(21, new List(321, new List(4321, singleton(54321)))))

  // If the input is an empty list, the output should be an empty list.

  // Your code should be *fully* pure: if you modify any variables after creation
  // you will not get full points. This means you'll need to use recursion.

  // You may create additional helper functions for this problem. Do not
  // modify any code in this file except for adding new helper function
  // definitions and implementing the cumulativeSum definition.

  static List cumulativeSum(final List l) {
    return null; // replace this line with your code
  }


  // EXERCISE 4.
  // Implement the "findIndexOfValue" function to return the first (leftmost)
  // index of the given value in the list.

  // For example,
  //   findIndexOfValue(11, new List(10, new List(11, new List(12, singleton(11)))))
  // should return
  //   MaybeInt(1)

  // You can assume that the length of the list is less than or equal to MAX_INT.

  // Your code should be *fully* pure: if you modify any variables after creation
  // you will not get full points. This means you'll need to use recursion.

  // You will need to create a subtype of MaybeIntUser<MaybeInt> in order to
  // complete this problem. Do not modify any code in this file except for adding
  // this new type definition and implementing the findIndexOfValue definition.
  // Also, do not use the toString function of the MaybeInt type.

  static MaybeInt findIndexOfValue(final int valueToFind, final List l) {
    return null; // replace this line with your code
  }



  // MAIN FUNCTION AND TEST CODE

  // This is a small utility function for the test code below.
  static void test(final String testCase, final boolean test) {
    if (!test)
      System.out.println(testCase + ": failed");
  }

  // Here's some concrete examples of how to construct and use the data types and
  // functions in this file. This main function also contains the automated tests
  // for your code: if you pass all these tests *and follow all the instructions*,
  // then you'll get 100% for the assignment. You are required to implement the
  // algorithms as specified so that they work correctly for all possible inputs
  // of the correct type, not just the specific test cases below.
  public static void main(String[] args) {
    // Feel free to add your own code here for your own testing, but please
    // remove it (or comment it out) before you submit your code so that it
    // doesn't interfere with the automated testing script. (You won't lose
    // points if you forget to do this, but you might make the person grading
    // your code grumpy.)

    // This is the list [1,2,3,4].
    List oneToFour = new List(1, new List(2, new List(3, singleton(4))));

    System.out.println("oneToFour =");
    recursivePrint(oneToFour);
    System.out.println("getValueAtIndex(1, oneToFour) = " + getValueAtIndex(1, oneToFour));
    System.out.println("getValueAtIndex(5, oneToFour) = " + getValueAtIndex(5, oneToFour));
    System.out.println();

    // Here's a semi-arbitrary list of values that we'll use for testing. Again,
    // your code must implement the algorithms as specified for all possible
    // values of the correct type, not just for this particular set of values.
    List testInts =
      new List(Integer.MIN_VALUE, new List(Integer.MIN_VALUE+1, new List(Integer.MAX_VALUE, new List(Integer.MAX_VALUE-1,
        new List(-2, new List(-1, new List(0, new List(1, new List(2, new List(3,
          new List(-1, new List(1234567, new List(Integer.MAX_VALUE-1, new List(-1234567,
            null))))))))))))));

    // An example of *property testing*, which we'll see much more of in Haskell:
    // For any int value and any list, if we prepend the int to the front of the
    // list and get the value at index 0, we should get that same int value back.
    // We'll test this for a range of integers over a small selection of lists.
    System.out.println("testing getValueAtIndex");
    for (List current = testInts; current != null; current = current.tail) {
      final int i = current.head;
      test("empty list",
        equals(getValueAtIndex(0, insertFirst(i, null)), new MaybeInt(i)));

      test("singleton list containing 0",
        equals(getValueAtIndex(0, insertFirst(i, singleton(0))), new MaybeInt(i)));

      test("singleton list containing " + i,
        equals(getValueAtIndex(0, insertFirst(i, singleton(i))), new MaybeInt(i)));

      test("arbitrary five-element list",
        equals(getValueAtIndex(0, insertFirst(i, new List(1, new List(-100, new List(-654321, new List(0, singleton(2))))))), new MaybeInt(i)));
    }
    System.out.println();

    // To test problem 1, we'll split testInts into two lists and append those
    // two lists together, and check that the result we get is equal to testInts.
    // We'll try every possible index to split at, including the two edge cases:
    //   prefix = null, suffix = testInts
    //   prefix = testInts, suffix = null
    // This gives us a total of *15* cases to test for a list of length 14.
    System.out.println("testing append (problem 1)");
    List prefix = null, suffix = testInts;
    for (int i = 0; i < 15; i++) {
      test("splitting testInts at index " + i,
        equals(append(prefix, suffix), testInts));

      if (suffix != null) { // false on the last loop iteration
        prefix = insertLast(prefix, suffix.head);
        suffix = suffix.tail;
      }
    }

    // To test problems 2, 3, and 4, we'll just hand-write a few test cases to
    // cover the edge cases and a couple arbitrary cases. Remember, your code
    // must work as specified for *all* possible inputs, not just these!
    //
    System.out.println("testing pairwiseAdd (problem 2)");

    test("both lists empty",
      equals(pairwiseAdd(null, null), null));

    test("l1 empty, l2 singleton",
      equals(pairwiseAdd(null, singleton(1)), null));

    test("l1 singleton, l2 empty",
      equals(pairwiseAdd(singleton(1), null), null));

    test("l1 three-element, l2 empty",
      equals(pairwiseAdd(new List(-1, new List(0, singleton(1))), null), null));

    test("l1 empty, l2 three-element",
      equals(pairwiseAdd(null, new List(-1, new List(0, singleton(1)))), null));

    test("l1 singleton, l2 singleton",
      equals(pairwiseAdd(singleton(-100), singleton(1)), singleton(-99)));

    test("l1 singleton, l2 three-element",
      equals(pairwiseAdd(singleton(-100), new List(-1, new List(0, singleton(1)))), singleton(-101)));

    test("l1 three-element, l2 singleton",
      equals(pairwiseAdd(singleton(-100), new List(-1, new List(0, singleton(1)))), singleton(-101)));

    test("both lists five-element",
      equals(
        pairwiseAdd(
          new List(1, new List(2, new List(3, new List(4, singleton(5))))),
          new List(10, new List(20, new List(30, new List(40, singleton(50)))))
        ),
        new List(11, new List(22, new List(33, new List(44, singleton(55)))))));

    test("l1 five-element, l2 six-element",
      equals(
        pairwiseAdd(
          new List(1, new List(2, new List(3, new List(4, singleton(5))))),
          new List(10, new List(20, new List(30, new List(40, new List(50, singleton(60))))))
        ),
        new List(11, new List(22, new List(33, new List(44, singleton(55)))))));

    test("l1 six-element, l2 five-element",
      equals(
        pairwiseAdd(
          new List(1, new List(2, new List(3, new List(4, new List(5, singleton(6)))))),
          new List(10, new List(20, new List(30, new List(40, singleton(50)))))
        ),
        new List(11, new List(22, new List(33, new List(44, singleton(55)))))));


    System.out.println("testing cumulativeSum (problem 3)");

    test("empty list",
      equals(cumulativeSum(null), null));

    test("negative singleton list",
      equals(cumulativeSum(singleton(-1)), singleton(-1)));

    test("zero singleton list",
      equals(cumulativeSum(singleton(0)), singleton(0)));

    test("positive singleton list",
      equals(cumulativeSum(singleton(1)), singleton(1)));

    test("arbitrary three-element list",
      equals(
        cumulativeSum(new List(1, new List(20, singleton(300)))),
        new List(1, new List(21, singleton(321)))));

    test("arbitrary fourteen-element list",
      equals(
        cumulativeSum(testInts),
        new List(-2147483648, new List(1, new List(-2147483648, new List(-2,
          new List(-4, new List(-5, new List(-5, new List(-4, new List(-2,
            new List(1, new List(0, new List(1234567, new List(-2146249083,
              singleton(2147483646))))))))))))))));


    System.out.println("testing findIndexOfValue (problem 4)");

    test("empty list, negative value",
      findIndexOfValue(-1, null).use(new NothingEqualsUser()));

    test("empty list, zero value",
      findIndexOfValue(0, null).use(new NothingEqualsUser()));

    test("empty list, positive value",
      findIndexOfValue(1, null).use(new NothingEqualsUser()));

    test("singleton list, found negative value",
      findIndexOfValue(-1, singleton(-1)).use(new JustEqualsUser(0)));

    test("singleton list, found positive value",
      findIndexOfValue(1, singleton(1)).use(new JustEqualsUser(0)));

    test("singleton list, not found minimum value",
      findIndexOfValue(Integer.MIN_VALUE, singleton(0)).use(new NothingEqualsUser()));

    test("three-element list, found value",
      findIndexOfValue(5, new List(1, new List(5, singleton(10)))).use(new JustEqualsUser(1)));

    test("three-element list, not found value",
      findIndexOfValue(8, new List(1, new List(5, singleton(10)))).use(new NothingEqualsUser()));

    test("fourteen-element list, found value near start",
      findIndexOfValue(Integer.MIN_VALUE+1, testInts).use(new JustEqualsUser(1)));

    test("fourteen-element list, found value near middle",
      findIndexOfValue(0, testInts).use(new JustEqualsUser(6)));

    test("fourteen-element list, found value near end",
      findIndexOfValue(1234567, testInts).use(new JustEqualsUser(11)));

    test("fourteen-element list, found duplicate value",
      findIndexOfValue(Integer.MAX_VALUE-1, testInts).use(new JustEqualsUser(3)));

    test("fourteen-element list, not found value",
      findIndexOfValue(54321, testInts).use(new NothingEqualsUser()));
  }
}
